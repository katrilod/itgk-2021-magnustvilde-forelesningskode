'''nøstede for-løkker'''

# lag et program som skriver ut tallene fra 1 til og med 9 i et 'rutenett':
'''
1    2    3
4    5    6
7    8    9
'''

#oppdatert løsning, ikke gjennomgått i forelesning
for i in range(3):
    string = ''
    for j in range(1,4):
        string += f'{i*3+j}\t'
    print(string)