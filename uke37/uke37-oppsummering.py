'''
UKE 37
Oppsummering
'''

# while-løkker
#betingelse med int
counter = 0
while counter<15:
    print(f'counter: {counter}')
    counter+=1

#betingelse med boolsk variabel
sant = True
while sant:
    svar = input('Skriv \'Ut\' for å gå ut: ')
    if svar == 'Ut':
        sant = False

# for-løkker, forskjellige måter å lage de på
#ett argument
for i in range(10):
    print(f'i: {i}')

#to argumenter
for x in range(10,20):
    print(f'x: {x}')

#tre argumenter
for x in range(10,20,2):
    print(f'x: {x}')

#nøstet løkke
for ute in range(1,4):
    for inne in range(1,4):
        print(ute*inne)
        
#for på streng
tekst = 'Min tekst!'
for bokstav in tekst:
    print(bokstav)

