'''logiske uttrykk'''

sant = True
usant = False

# bruk operatoren and
og = sant and usant
print(og)

# bruk operatoren or
eller = sant or usant
print(eller)

# bruk operatoren not
ikke = not sant
ikkeIgjen = not usant
print(f'Sant: {ikke}')
print(f'Usant: {ikkeIgjen}')

# sett sammen flere operatorer
flere = (sant and usant) or (not usant)
print(f'Flere satt sammen: {flere}')

# lag en variabel som representerer alder, og lag en boolsk variabel
#trafikaltGrunnkurs. Lag forskjellige kombinasjoner, der du skriver
#hva man kan gjøre med de forskjellige kombinasjonene
#(kjøpe alkohol, ta førerkortet, lærekjøre, etc)

alder = 18
trafikaltGrunnkurs = True
if (alder>=18 and trafikaltGrunnkurs):
    print('Du kan ta førerkortet!')
elif (alder >= 18 and not trafikaltGrunnkurs):
    print('Du må først ta trafikalt grunnkurs før du kan ta lappen!')
# elif (alder < 18)